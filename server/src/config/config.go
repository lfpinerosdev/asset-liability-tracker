package config

import (
	"encoding/json"
	"errors"
	"log"
	"os"
)

var Server serverConfiguration
var DB databaseConfiguration

type serverConfiguration struct {
	Port string `json:"port"`
}

type databaseConfiguration struct {
	Port     string `json:"port"`
	Host     string `json:"host"`
	User     string `json:"user"`
	Password string `json:"password"`
	Database string `json:"database"`
}

func Load() error {

	if err := loadServerConfig(); err != nil {
		return err
	}

	if err := loadDBConfig(); err != nil {
		return err
	}

	return nil
}

func loadServerConfig() error {
	log.Println("Loading server configurations...")

	bytes := []byte(os.Getenv("SERVER_PROPERTIES"))
	if err := json.Unmarshal(bytes, &Server); err != nil {
		return err
	}

	if Server.Port == "" {
		return errors.New("mandatory configuration missing for server")
	}

	log.Println("Server configurations loaded")
	return nil
}

func loadDBConfig() error {
	log.Println("Loading database configurations...")

	bytes := []byte(os.Getenv("DATABASE_PROPERTIES"))
	if err := json.Unmarshal(bytes, &DB); err != nil {
		return err
	}

	if DB.Host == "" || DB.Port == "" || DB.User == "" || DB.Password == "" || DB.Database == "" {
		return errors.New("mandatory configuration missing for database")
	}

	log.Println("Database configurations loaded")
	return nil
}
