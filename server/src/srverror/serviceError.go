package srverror

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

type ErrorResponse struct {
	Details    string `json:"details"`
	Code       string `json:"code"`
	StatusCode int    `json:"statusCode"`
}

type serviceError struct {
	errorCode ErrorCode
	message   string
	params    []interface{}
	details   string
}

func (e serviceError) Error() string {
	return e.message
}

// newServiceTypeError returns a serviceError intended to be used for non-service errors (e.g db errors)
func newServiceTypeError(errorCode ErrorCode, err error) serviceError {
	return serviceError{
		errorCode: errorCode,
		details:   err.Error(),
	}
}

// New returns a service error object as error type
func New(errorCode ErrorCode) error {
	return serviceError{
		errorCode: errorCode,
	}
}

// Newf returns a service error object as error type with params
func Newf(errorCode ErrorCode, params ...interface{}) error {
	return serviceError{
		errorCode: errorCode,
		params:    params,
	}
}

func Handle(w http.ResponseWriter, err error) {
	serviceErr, ok := err.(serviceError)
	if !ok {
		serviceErr = newServiceTypeError(UnhandledError, err)
	}

	response := getErrorResponse(serviceErr)

	logError(err, response)

	sendError(w, response)
}

func getErrorResponse(serviceErr serviceError) ErrorResponse {
	switch serviceErr.errorCode {
	case NameIsRequiredError:
		return ErrorResponse{Details: "Name is a required field", Code: string(NameIsRequiredError), StatusCode: http.StatusBadRequest}
	case NameMaxLengthError:
		detail := fmt.Sprintf("Name exceeds max length of %d characters", serviceErr.params)
		return ErrorResponse{Details: detail, Code: string(NameMaxLengthError), StatusCode: http.StatusBadRequest}
	case TypeIsNotValidError:
		return ErrorResponse{Details: "Type is not valid", Code: string(TypeIsNotValidError), StatusCode: http.StatusBadRequest}
	case FinanceItemDoesNotExistError:
		return ErrorResponse{Details: "Finance Item does not exist", Code: string(FinanceItemDoesNotExistError), StatusCode: http.StatusNotFound}
	case UnhandledError:
		return ErrorResponse{Details: "Unexpected error", Code: string(UnhandledError), StatusCode: http.StatusInternalServerError}
	default:
		return ErrorResponse{Details: "Unexpected error", Code: string(UnhandledError), StatusCode: http.StatusInternalServerError}
	}
}

func logError(err error, response ErrorResponse) {
	if err.Error() == "" {
		log.Println(response.Details)
	} else {
		log.Println(err.Error())
	}
}

func sendError(w http.ResponseWriter, errorResponse ErrorResponse) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(errorResponse.StatusCode)
	json.NewEncoder(w).Encode(errorResponse)
}
