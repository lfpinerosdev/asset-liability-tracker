package srverror

type ErrorCode string

const (
	UnhandledError               ErrorCode = "UnhandledError"
	NameIsRequiredError          ErrorCode = "NameIsRequiredError"
	NameMaxLengthError           ErrorCode = "NameMaxLengthError"
	FinanceItemDoesNotExistError ErrorCode = "FinanceItemDoesNotExistError"
	TypeIsNotValidError          ErrorCode = "TypeIsNotValidError"
)
