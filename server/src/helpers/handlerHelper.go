package helpers

import (
	"encoding/json"
	"net/http"
	"server/src/srverror"
)

// EncodeResponse turns the response into json and writes it back to the ResponseWriter
func EncodeResponse(w http.ResponseWriter, i interface{}) {
	err := json.NewEncoder(w).Encode(&i)
	if err != nil {
		srverror.Handle(w, err)
	}
}

// DecodeRequest decodes the request
func DecodeRequest(r *http.Request, i interface{}) error {
	decoder := json.NewDecoder(r.Body)
	return decoder.Decode(&i)
}
