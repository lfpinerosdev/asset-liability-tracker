package helpers

import "math"

func RoundOff(x float64) float64 {
	return math.Round(x*100) / 100
}
