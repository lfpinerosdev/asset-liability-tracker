package main

import (
	"log"
	"net/http"
	"server/src/config"
	"server/src/db"
	"server/src/entity/balancesheet"
	"server/src/entity/financeitem"
	"strings"

	"github.com/gorilla/mux"
	"github.com/rs/cors"
)

func main() {

	err := config.Load()
	if err != nil {
		log.Fatal(err)
		return
	}

	err = db.Load()
	if err != nil {
		log.Fatal(err)
		return
	}

	injectDependencies()

	r := mux.NewRouter()
	r = r.PathPrefix("/api").Subrouter()

	r.Use(headerMiddleWare)

	setRoutes(r)

	handler := setCors(r)

	log.Println("Server listening on port:" + config.Server.Port)
	http.ListenAndServe(":"+config.Server.Port, removeTrailingSlashMiddleware(handler))
}

func injectDependencies() {
	balancesheet.FinanceItemService = financeitem.FinanceItemServiceImpl{}
}

// removeTrailingSlashMiddleware removes trailing "/" from API requests
func removeTrailingSlashMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		r.URL.Path = strings.TrimSuffix(r.URL.Path, "/")
		next.ServeHTTP(w, r)
	})
}

// headerMiddleware adds response headers before calling the controller
func headerMiddleWare(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "application/json; charset=utf-8")

		next.ServeHTTP(w, r)
	})
}

func setCors(r http.Handler) http.Handler {
	c := cors.New(cors.Options{
		AllowedOrigins:   []string{"http://localhost:3000", "https://kind-mud-0effc870f.azurestaticapps.net"},
		AllowCredentials: true,
		AllowedMethods:   []string{"POST", "DELETE", "OPTIONS", "GET"},
	})

	return c.Handler(r)
}

func setRoutes(r *mux.Router) {
	r.HandleFunc("/finance-item", financeitem.HandleFinanceItemAdd).Methods(http.MethodPost)
	r.HandleFunc("/finance-item/{id:[0-9]+}", financeitem.HandleFinanceItemDelete).Methods(http.MethodDelete)
	r.HandleFunc("/balance-sheet", balancesheet.HandleBalanceSheetGet).Methods(http.MethodGet)
}
