package db

import (
	"database/sql"
	"fmt"
	"log"
	"server/src/config"

	_ "github.com/go-sql-driver/mysql"
)

var db *sql.DB

func Load() error {
	log.Println("Connecting to Database...")

	newDB, err := sql.Open("mysql", buildConnectionString())
	if err != nil {
		return err
	}
	db = newDB

	err = db.Ping()
	if err != nil {
		return err
	}

	log.Println("Connected to Database")
	return nil
}

// Insert will insert one record into the db and return the id or error
func Insert(query string, args ...interface{}) (int64, error) {
	res, err := db.Exec(query, args...)
	if err != nil {
		return -1, err
	}

	return res.LastInsertId()
}

// Exec will execute the query and return if any errors
func Exec(query string, args ...interface{}) error {
	_, err := db.Exec(query, args...)
	if err != nil {
		return err
	}

	return nil
}

//Query will query and return multiple rows
func Query(query string, args []interface{}) (*sql.Rows, error) {
	return db.Query(query, args...)
}

// QueryOne will query the db and return one row
// result parameter is the fields to map from the result. should be passed by reference
func QueryOne(query string, args []interface{}, result []interface{}) error {
	return db.QueryRow(query, args...).Scan(result...)
}

func buildConnectionString() string {
	db := config.DB.Database
	user := config.DB.User
	host := config.DB.Host
	port := config.DB.Port
	password := config.DB.Password

	connectionString := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", user, password, host, port, db)

	return connectionString
}
