package model

type BalanceSheet struct {
	Assets                  []FinanceItem `json:"assets"`
	Liabilities             []FinanceItem `json:"liabilities"`
	NetWorth                float64       `json:"netWorth"`
	TotalAssetsBalance      float64       `json:"totalAssetsBalance"`
	TotalLiabilitiesBalance float64       `json:"totalLiabilitiesBalance"`
}
