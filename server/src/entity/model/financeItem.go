package model

type FinanceItem struct {
	ID      int64           `json:"id"`
	Type    FinanceItemType `json:"type"`
	Name    string          `json:"name"`
	Balance float64         `json:"balance"`
}

func NewFinanceItem(name string, itemType FinanceItemType, balance float64) FinanceItem {
	return FinanceItem{
		Name:    name,
		Type:    itemType,
		Balance: balance,
	}
}
