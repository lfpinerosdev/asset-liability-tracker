package model

type FinanceItemType string

const (
	Asset     FinanceItemType = "Asset"
	Liability FinanceItemType = "Liability"
	Empty     FinanceItemType = ""
)

func (f FinanceItemType) IsValid() bool {
	if f == Asset || f == Liability {
		return true
	}

	return false
}

func (f FinanceItemType) String() string {
	return string(f)
}
