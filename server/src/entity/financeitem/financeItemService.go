package financeitem

import (
	"server/src/entity/model"
	"server/src/helpers"
	"server/src/srverror"
	"strings"
)

var (
	roundOff = helpers.RoundOff
)

const (
	NameMaxLength = 100
)

type FinanceItemServiceImpl struct{}

type FinanceItemService interface {
	AddFinanceItem(item model.FinanceItem) (model.FinanceItem, error)
	GetFinanceItems() ([]model.FinanceItem, error)
	DeleteFinanceItem(id int) error
}

func (f FinanceItemServiceImpl) AddFinanceItem(item model.FinanceItem) (model.FinanceItem, error) {
	err := validate(item)
	if err != nil {
		return model.FinanceItem{}, err
	}

	item.Balance = roundOff(item.Balance)

	return addFinanceItem(item)
}

func (f FinanceItemServiceImpl) GetFinanceItems() ([]model.FinanceItem, error) {
	return getFinanceItems()
}

func (f FinanceItemServiceImpl) DeleteFinanceItem(id int) error {
	exists, err := exists(id)
	if err != nil {
		return err
	}

	if !exists {
		return srverror.New(srverror.FinanceItemDoesNotExistError)
	}

	err = deleteFinanceItem(id)
	if err != nil {
		return err
	}

	return nil
}

func validate(item model.FinanceItem) error {

	if !item.Type.IsValid() {
		return srverror.New(srverror.TypeIsNotValidError)
	}

	if isEmpty(item.Name) {
		return srverror.New(srverror.NameIsRequiredError)
	}

	if len(item.Name) > NameMaxLength {
		return srverror.Newf(srverror.NameMaxLengthError, srverror.NameMaxLengthError)
	}

	return nil
}

func isEmpty(s string) bool {
	return strings.TrimSpace(s) == ""
}
