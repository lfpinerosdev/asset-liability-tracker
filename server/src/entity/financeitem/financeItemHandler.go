package financeitem

import (
	"log"
	"net/http"
	"server/src/entity/model"
	"server/src/helpers"
	"server/src/srverror"
	"strconv"

	"github.com/gorilla/mux"
)

var (
	encodeResponse = helpers.EncodeResponse
	decodeRequest  = helpers.DecodeRequest
)

func HandleFinanceItemAdd(w http.ResponseWriter, r *http.Request) {
	log.Println("Handling add finance item")

	item := model.FinanceItem{}

	decodeRequest(r, &item)
	newItem, err := FinanceItemServiceImpl{}.AddFinanceItem(item)
	if err != nil {
		srverror.Handle(w, err)
		return
	}

	w.WriteHeader(http.StatusCreated)
	encodeResponse(w, &newItem)
	log.Println("Completed add finance item")
}

func HandleFinanceItemDelete(w http.ResponseWriter, r *http.Request) {
	log.Println("Handling delete finance item")

	id, err := strconv.Atoi(mux.Vars(r)["id"])
	if err != nil {
		srverror.Handle(w, err)
		return
	}

	err = FinanceItemServiceImpl{}.DeleteFinanceItem(id)
	if err != nil {
		srverror.Handle(w, err)
		return
	}

	w.WriteHeader(http.StatusNoContent)
	log.Println("Completed delete finance item")
}
