package financeitem

import (
	"server/src/db"
	"server/src/entity/model"
)

const (
	insertQuery = "INSERT INTO finance_item(name, type, balance) VALUES (?, ?, ?);"

	selectQuery = "SELECT id, name, type, balance FROM finance_item"

	existsQuery = "SELECT COUNT(1) FROM finance_item WHERE id = ?;"

	deleteQuery = "DELETE FROM finance_item WHERE id = ?;"
)

func addFinanceItem(item model.FinanceItem) (model.FinanceItem, error) {
	id, err := db.Insert(insertQuery, item.Name, item.Type, item.Balance)
	if err != nil {
		return model.FinanceItem{}, err
	}
	item.ID = id
	return item, nil
}

func getFinanceItems() ([]model.FinanceItem, error) {
	financeItem := model.FinanceItem{}

	rows, err := db.Query(selectQuery, nil)
	if err != nil {
		return nil, err
	}

	items := make([]model.FinanceItem, 0)
	for rows.Next() {
		err = rows.Scan(&financeItem.ID, &financeItem.Name, &financeItem.Type, &financeItem.Balance)
		if err != nil {
			return nil, err
		}

		items = append(items, financeItem)
	}

	return items, nil
}

func deleteFinanceItem(id int) error {
	return db.Exec(deleteQuery, id)
}

func exists(id int) (bool, error) {
	var exists bool

	args := []interface{}{id}
	results := []interface{}{&exists}

	err := db.QueryOne(existsQuery, args, results)
	if err != nil {
		return false, err
	}

	return exists, nil
}
