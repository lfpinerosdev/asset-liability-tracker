package balancesheet

import (
	"server/src/entity/financeitem"
	"server/src/entity/model"
	"server/src/helpers"
)

var (
	roundOff = helpers.RoundOff
)

var FinanceItemService financeitem.FinanceItemService = nil

func GetBalanceSheet() (model.BalanceSheet, error) {
	items, err := FinanceItemService.GetFinanceItems()
	if err != nil {
		return model.BalanceSheet{}, err
	}

	balanceSheet := model.BalanceSheet{}

	fillBalanceSheet(items, &balanceSheet)

	return balanceSheet, nil
}

func fillBalanceSheet(items []model.FinanceItem, balanceSheet *model.BalanceSheet) {
	balanceSheet.NetWorth = 0.00
	balanceSheet.TotalAssetsBalance = 0.00
	balanceSheet.TotalLiabilitiesBalance = 0.00

	assets := make([]model.FinanceItem, 0)
	liabilities := make([]model.FinanceItem, 0)

	for _, item := range items {
		switch item.Type {
		case model.Asset:
			balanceSheet.TotalAssetsBalance += item.Balance
			assets = append(assets, item)
		case model.Liability:
			balanceSheet.TotalLiabilitiesBalance += item.Balance
			liabilities = append(liabilities, item)
		}
	}

	balanceSheet.Assets = assets
	balanceSheet.Liabilities = liabilities
	balanceSheet.TotalAssetsBalance = roundOff(balanceSheet.TotalAssetsBalance)
	balanceSheet.TotalLiabilitiesBalance = roundOff(balanceSheet.TotalLiabilitiesBalance)
	balanceSheet.NetWorth = roundOff(balanceSheet.TotalAssetsBalance - balanceSheet.TotalLiabilitiesBalance)
}
