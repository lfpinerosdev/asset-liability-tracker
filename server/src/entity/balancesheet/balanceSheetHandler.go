package balancesheet

import (
	"log"
	"net/http"
	"server/src/helpers"
	"server/src/srverror"
)

var (
	encodeResponse = helpers.EncodeResponse
	decodeRequest  = helpers.DecodeRequest
)

func HandleBalanceSheetGet(w http.ResponseWriter, r *http.Request) {
	log.Println("Handling get balance sheet")

	balanceSheet, err := GetBalanceSheet()
	if err != nil {
		srverror.Handle(w, err)
		return
	}

	encodeResponse(w, &balanceSheet)
	log.Println("Completed get balance sheet")
}
