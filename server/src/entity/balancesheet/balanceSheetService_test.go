package balancesheet

import (
	"server/src/entity/financeitem"
	"server/src/entity/model"
	"server/src/mocks"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/google/go-cmp/cmp"
)

func TestGetBalanceSheet(t *testing.T) {
	defer func(v financeitem.FinanceItemService) {
		FinanceItemService = v
	}(FinanceItemService)

	type testArgs struct {
		testScenario         string
		financialItems       []model.FinanceItem
		expectedBalanceSheet model.BalanceSheet
		setupMocks           func(ctrl *gomock.Controller)
	}

	tests := []testArgs{
		{
			testScenario: "sum_assets_no_liabilities",
			financialItems: []model.FinanceItem{
				model.NewFinanceItem("asset", model.Asset, 1000),
				model.NewFinanceItem("asset", model.Asset, 1000),
			},
			expectedBalanceSheet: model.BalanceSheet{
				Assets: []model.FinanceItem{
					model.NewFinanceItem("asset", model.Asset, 1000),
					model.NewFinanceItem("asset", model.Asset, 1000),
				},
				Liabilities:             make([]model.FinanceItem, 0),
				NetWorth:                2000,
				TotalAssetsBalance:      2000,
				TotalLiabilitiesBalance: 0,
			},
			setupMocks: func(ctrl *gomock.Controller) {
				financeItemServiceMock := mocks.NewMockFinanceItemService(ctrl)
				financeItemServiceMock.EXPECT().GetFinanceItems().Times(1).Return([]model.FinanceItem{
					model.NewFinanceItem("asset", model.Asset, 1000),
					model.NewFinanceItem("asset", model.Asset, 1000),
				}, nil)

				FinanceItemService = financeItemServiceMock
			},
		},
		{
			testScenario: "sum_liabilities_no_assets",
			financialItems: []model.FinanceItem{
				model.NewFinanceItem("liability", model.Liability, 1000),
				model.NewFinanceItem("liability", model.Liability, 1000),
			},
			expectedBalanceSheet: model.BalanceSheet{
				Assets: make([]model.FinanceItem, 0),
				Liabilities: []model.FinanceItem{
					model.NewFinanceItem("liability", model.Liability, 1000),
					model.NewFinanceItem("liability", model.Liability, 1000),
				},
				NetWorth:                -2000,
				TotalAssetsBalance:      0,
				TotalLiabilitiesBalance: 2000,
			},
			setupMocks: func(ctrl *gomock.Controller) {
				financeItemServiceMock := mocks.NewMockFinanceItemService(ctrl)
				financeItemServiceMock.EXPECT().GetFinanceItems().Times(1).Return([]model.FinanceItem{
					model.NewFinanceItem("liability", model.Liability, 1000),
					model.NewFinanceItem("liability", model.Liability, 1000),
				}, nil)

				FinanceItemService = financeItemServiceMock
			},
		},
		{
			testScenario: "positive_networth_from_mix",
			financialItems: []model.FinanceItem{
				model.NewFinanceItem("assets", model.Asset, 1000),
				model.NewFinanceItem("assets", model.Asset, 1000),
				model.NewFinanceItem("assets", model.Asset, 1000),
				model.NewFinanceItem("liability", model.Liability, 500),
				model.NewFinanceItem("liability", model.Liability, 500),
				model.NewFinanceItem("liability", model.Liability, 500),
			},
			expectedBalanceSheet: model.BalanceSheet{
				Assets: []model.FinanceItem{
					model.NewFinanceItem("assets", model.Asset, 1000),
					model.NewFinanceItem("assets", model.Asset, 1000),
					model.NewFinanceItem("assets", model.Asset, 1000),
				},
				Liabilities: []model.FinanceItem{
					model.NewFinanceItem("liability", model.Liability, 500),
					model.NewFinanceItem("liability", model.Liability, 500),
					model.NewFinanceItem("liability", model.Liability, 500),
				},
				NetWorth:                1500,
				TotalAssetsBalance:      3000,
				TotalLiabilitiesBalance: 1500,
			},
			setupMocks: func(ctrl *gomock.Controller) {
				financeItemServiceMock := mocks.NewMockFinanceItemService(ctrl)
				financeItemServiceMock.EXPECT().GetFinanceItems().Times(1).Return([]model.FinanceItem{
					model.NewFinanceItem("assets", model.Asset, 1000),
					model.NewFinanceItem("assets", model.Asset, 1000),
					model.NewFinanceItem("assets", model.Asset, 1000),
					model.NewFinanceItem("liability", model.Liability, 500),
					model.NewFinanceItem("liability", model.Liability, 500),
					model.NewFinanceItem("liability", model.Liability, 500),
				}, nil)

				FinanceItemService = financeItemServiceMock
			},
		},
		{
			testScenario: "negative_networth_from_mix",
			financialItems: []model.FinanceItem{
				model.NewFinanceItem("assets", model.Asset, 1000),
				model.NewFinanceItem("assets", model.Asset, 1000),
				model.NewFinanceItem("assets", model.Asset, 1000),
				model.NewFinanceItem("liability", model.Liability, 1000),
				model.NewFinanceItem("liability", model.Liability, 2000),
				model.NewFinanceItem("liability", model.Liability, 3000),
			},
			expectedBalanceSheet: model.BalanceSheet{
				Assets: []model.FinanceItem{
					model.NewFinanceItem("assets", model.Asset, 1000),
					model.NewFinanceItem("assets", model.Asset, 1000),
					model.NewFinanceItem("assets", model.Asset, 1000),
				},
				Liabilities: []model.FinanceItem{
					model.NewFinanceItem("liability", model.Liability, 1000),
					model.NewFinanceItem("liability", model.Liability, 2000),
					model.NewFinanceItem("liability", model.Liability, 3000),
				},
				NetWorth:                -3000,
				TotalAssetsBalance:      3000,
				TotalLiabilitiesBalance: 6000,
			},
			setupMocks: func(ctrl *gomock.Controller) {
				financeItemServiceMock := mocks.NewMockFinanceItemService(ctrl)
				financeItemServiceMock.EXPECT().GetFinanceItems().Times(1).Return([]model.FinanceItem{
					model.NewFinanceItem("assets", model.Asset, 1000),
					model.NewFinanceItem("assets", model.Asset, 1000),
					model.NewFinanceItem("assets", model.Asset, 1000),
					model.NewFinanceItem("liability", model.Liability, 1000),
					model.NewFinanceItem("liability", model.Liability, 2000),
					model.NewFinanceItem("liability", model.Liability, 3000),
				}, nil)

				FinanceItemService = financeItemServiceMock
			},
		},
		{
			testScenario: "decimal_positive_networth",
			financialItems: []model.FinanceItem{
				model.NewFinanceItem("assets", model.Asset, 859.19),
				model.NewFinanceItem("assets", model.Asset, 8960.14),
				model.NewFinanceItem("assets", model.Asset, 5197.98),
				model.NewFinanceItem("assets", model.Asset, 3698.75),
				model.NewFinanceItem("liability", model.Liability, 6793.14),
				model.NewFinanceItem("liability", model.Liability, 749.78),
			},
			expectedBalanceSheet: model.BalanceSheet{
				Assets: []model.FinanceItem{
					model.NewFinanceItem("assets", model.Asset, 859.19),
					model.NewFinanceItem("assets", model.Asset, 8960.14),
					model.NewFinanceItem("assets", model.Asset, 5197.98),
					model.NewFinanceItem("assets", model.Asset, 3698.75),
				},
				Liabilities: []model.FinanceItem{
					model.NewFinanceItem("liability", model.Liability, 6793.14),
					model.NewFinanceItem("liability", model.Liability, 749.78),
				},
				NetWorth:                11173.14,
				TotalAssetsBalance:      18716.06,
				TotalLiabilitiesBalance: 7542.92,
			},
			setupMocks: func(ctrl *gomock.Controller) {
				financeItemServiceMock := mocks.NewMockFinanceItemService(ctrl)
				financeItemServiceMock.EXPECT().GetFinanceItems().Times(1).Return([]model.FinanceItem{
					model.NewFinanceItem("assets", model.Asset, 859.19),
					model.NewFinanceItem("assets", model.Asset, 8960.14),
					model.NewFinanceItem("assets", model.Asset, 5197.98),
					model.NewFinanceItem("assets", model.Asset, 3698.75),
					model.NewFinanceItem("liability", model.Liability, 6793.14),
					model.NewFinanceItem("liability", model.Liability, 749.78),
				}, nil)

				FinanceItemService = financeItemServiceMock
			},
		},
		{
			testScenario: "decimal_negative_networth",
			financialItems: []model.FinanceItem{
				model.NewFinanceItem("assets", model.Asset, 150.19),
				model.NewFinanceItem("assets", model.Asset, 853.14),
				model.NewFinanceItem("assets", model.Asset, 5197.98),
				model.NewFinanceItem("liability", model.Liability, 6793.14),
				model.NewFinanceItem("liability", model.Liability, 749.78),
			},
			expectedBalanceSheet: model.BalanceSheet{
				Assets: []model.FinanceItem{
					model.NewFinanceItem("assets", model.Asset, 150.19),
					model.NewFinanceItem("assets", model.Asset, 853.14),
					model.NewFinanceItem("assets", model.Asset, 5197.98),
				},
				Liabilities: []model.FinanceItem{
					model.NewFinanceItem("liability", model.Liability, 6793.14),
					model.NewFinanceItem("liability", model.Liability, 749.78),
				},
				NetWorth:                -1341.61,
				TotalAssetsBalance:      6201.31,
				TotalLiabilitiesBalance: 7542.92,
			},
			setupMocks: func(ctrl *gomock.Controller) {
				financeItemServiceMock := mocks.NewMockFinanceItemService(ctrl)
				financeItemServiceMock.EXPECT().GetFinanceItems().Times(1).Return([]model.FinanceItem{
					model.NewFinanceItem("assets", model.Asset, 150.19),
					model.NewFinanceItem("assets", model.Asset, 853.14),
					model.NewFinanceItem("assets", model.Asset, 5197.98),
					model.NewFinanceItem("liability", model.Liability, 6793.14),
					model.NewFinanceItem("liability", model.Liability, 749.78),
				}, nil)

				FinanceItemService = financeItemServiceMock
			},
		},
	}

	for _, test := range tests {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		test.setupMocks(ctrl)

		results, _ := GetBalanceSheet()

		if !cmp.Equal(results, test.expectedBalanceSheet) {
			t.Errorf("\nScenario: %s\nwant: %v\ngot: %v", test.testScenario, test.expectedBalanceSheet, results)
		}
	}
}
