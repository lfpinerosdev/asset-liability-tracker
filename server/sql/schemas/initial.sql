CREATE DATABASE IF NOT EXISTS asset_liability_tracker;

USE asset_liability_tracker;

CREATE TABLE IF NOT EXISTS finance_item(
    id INT NOT NULL AUTO_INCREMENT,
    name TEXT NOT NULL,
    type ENUM('Asset', 'Liability') NOT NULL,
    balance DECIMAL(13,2) NOT NULL,
    PRIMARY KEY(id)
);