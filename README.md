# asset-liability-tracker

## Development

### Client Side
1. open client folder as working directory
    - execute: `npm start`

### Server 
1. open repo at the root level where docker-compose is
2. copy .env.example to .env file
3. set the environment variables for database
4. start application
    - execute: `docker-compose up`
5. when database is running, manually run initial.sql script inside container
